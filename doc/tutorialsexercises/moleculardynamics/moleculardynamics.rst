.. _moleculardynamics:

==================
Molecular dynamics
==================

.. toctree::
   :maxdepth: 2

   neb/neb
   diffusion/diffusion
   ehrenfest/ehrenfest

You might also be interested in these ASE tutorials:

:ref:`Molecular dynamics <ase:md_tutorial>`
:ref:`Nudged elastic band calculations <ase:selfdiffusion>`
